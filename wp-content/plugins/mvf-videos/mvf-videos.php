<?php
/**
 * @package   MVF Videos
 * @author    Hamza Abd El-Wahab [hubahamza.me]
 * @license   LICENSE.txt
 * @link      http://www.hubahamza.me
 *
 * Plugin Name: MVF Videos
 * Plugin URI: http://www.hubahamza.me
 * Description: Insert Youtube, Vimeo and Dailymotion videos directly anywhere in your posts/pages or use a shortcode to insert it anywhere.
 * Author: Hamza Abd El-Wahab
 * Version: 1
 * Author URI: http://www.hubahamza.me
 * License URI:       license.txt
 * Domain Path:       /languages
 * Tested up to: 	  4.9.2
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die('No Naughty Business Please !');
}

// Abort loading if WordPress is upgrading
if ( defined( 'WP_INSTALLING' ) && WP_INSTALLING ) {
    return;
}

function mvf_two_instance_notice() {
    echo '<div class="error"><p>' . __( 'You are using two instances of MVF Videos plugin at same time, please deactive one of them.', 'mvf-videos' ) . '</p></div>';
}

// check whether another instance of MVF Videos is activated or not
if( defined( 'MVFVIDEOS_VERSION' ) ){
	add_action( 'admin_notices', 'mvfvideos_two_instance_notice' );
	return;
}

/*----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'includes/init/define.php' );
require_once( plugin_dir_path( __FILE__ ) . 'admin/class-mvf-videos.php' );
require_once( plugin_dir_path( __FILE__ ) . 'public/class-mvf-display.php' );

/*----------------------------------------------------------------------------*/