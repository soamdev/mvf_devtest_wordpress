<?php

// no direct access allowed
if ( ! defined('ABSPATH') ) {
    die();
}

define( 'MVFVIDEOS_VERSION'		, '1.0.0' );

define( 'MVFVIDEOS_TEXT_DOMAIN'			, 'mvcvideos' );

define( 'MVFVIDEOS_DIR'			, dirname( dirname( plugin_dir_path( __FILE__ ) ) ) );
define( 'MVFVIDEOS_URL'			, plugins_url( '', dirname( plugin_dir_path( __FILE__ ) ) ) );
define( 'MVFVIDEOS_BASE_NAME'		, plugin_basename( MVFVIDEOS_DIR ) . '/mvf-videos.php' );


define( 'MVFVIDEOS_ADMIN_DIR'		, MVFVIDEOS_DIR . '/admin' );
define( 'MVFVIDEOS_ADMIN_URL'		, MVFVIDEOS_URL . '/admin' );

define( 'MVFVIDEOS_INC_DIR'		, MVFVIDEOS_DIR . '/includes' );
define( 'MVFVIDEOS_INC_URL'		, MVFVIDEOS_URL . '/includes' );

define( 'MVFVIDEOS_PUB_DIR'		, MVFVIDEOS_DIR . '/public' );
define( 'MVFVIDEOS_PUB_URL'		, MVFVIDEOS_URL . '/public' );

define( 'MVFVIDEOS_BLANK_IMG' 			, MVFVIDEOS_PUB_URL . '/assets/css/blank.gif' );

define( 'MVFVIDEOS_FEED_URL'		, '' );