<?php
/**
 * MVF Videos
 *
 * @package   MVF Videos
 * @author    Hamza Abd El-Wahab [hubahamza.me]
 * @license   LICENSE.txt
 * @link      http://hubahamza.me
 * @copyright Copyright © 2018 Hamza Abd El-Wahab
 */

if ( ! class_exists( 'MVF_DISPLAY' ) ) :

/**
 * Plugin class. This class should ideally be used to work with the
 * public-facing side of the WordPress site.
 */
class MVF_DISPLAY {
    
  /**
   * Instance of this class.
   *
   * @since    1.0.0
   *
   * @var      object
   */
  protected static $instance = null;


  /**
   *
   * @since    1.0.0
   *
   * @var      object
   */
  public $admin = null;

  /**
   * Initialize the plugin
   *
   * @since     1.0.0
   */

    public function init(){
        add_action( 'wp_enqueue_scripts', array($this, 'embed_scripts') );   
    }
    public function embed_scripts() {
        wp_enqueue_style( 'mvf-video-scripts', plugins_url('/assets/css/style.css' , __FILE__ ) );
    }

}

endif;

$MVFDISPLAY = new MVF_DISPLAY;
$MVFDISPLAY->init();