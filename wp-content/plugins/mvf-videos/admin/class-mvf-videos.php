<?php
/**
 * MVF Videos
 *
 * @package   MVF Videos
 * @author    Hamza Abd El-Wahab [hubahamza.me]
 * @license   LICENSE.txt
 * @link      http://hubahamza.me
 * @copyright Copyright © 2018 Hamza Abd El-Wahab
 */

if ( ! class_exists( 'MVF_VIDEOS' ) ) :

/**
 * Plugin class. This class should ideally be used to work with the
 * admin-facing side of the WordPress site.
 */
class MVF_VIDEOS {
    
  /**
   * Instance of this class.
   *
   * @since    1.0.0
   *
   * @var      object
   */
  protected static $instance = null;

  /**
   * Initialize the plugin
   *
   * @since     1.0.0
   */

    public function init(){
        add_action( 'init', array( $this, 'add_custom_post' ) );
        add_filter( 'mce_external_plugins', array($this, 'add_button') );
        add_filter( 'mce_buttons', array($this, 'register_button') );
        add_action( 'save_post', array($this, 'mvf_save_video'), 10, 2 );
        add_action( 'load-post.php', array($this, 'add_meta_boxes') );
        add_action( 'load-post-new.php', array($this, 'add_meta_boxes') );
        add_shortcode( 'prefix_video', array($this, 'add_shortcode' ));
        add_filter('mce_external_languages', array($this, 'mvf_tinymce_plugin_add_locale' ));
        add_action( 'save_post', array( $this, 'modify_post_content' ) );
    }
    public function add_custom_post(){
        $labels = array(
            'name'                  => _x( 'Videos', 'Post Type General Name', 'mvf-videos' ),
            'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'mvf-videos' ),
            'menu_name'             => __( 'Videos', 'mvf-videos' ),
            'name_admin_bar'        => __( 'Videos', 'mvf-videos' ),
            'archives'              => __( 'Video Archives', 'mvf-videos' ),
            'attributes'            => __( 'Video Attributes', 'mvf-videos' ),
            'parent_item_colon'     => __( 'Parent Video:', 'mvf-videos' ),
            'all_items'             => __( 'All Videos', 'mvf-videos' ),
            'add_new_item'          => __( 'Add New Video', 'mvf-videos' ),
            'add_new'               => __( 'Add New Video', 'mvf-videos' ),
            'new_item'              => __( 'New Video', 'mvf-videos' ),
            'edit_item'             => __( 'Edit Video', 'mvf-videos' ),
            'update_item'           => __( 'Update Video', 'mvf-videos' ),
            'view_item'             => __( 'View Video', 'mvf-videos' ),
            'view_items'            => __( 'View Videos', 'mvf-videos' ),
            'search_items'          => __( 'Search Video', 'mvf-videos' ),
            'not_found'             => __( 'Video Not found', 'mvf-videos' ),
            'not_found_in_trash'    => __( 'Video Not found in Trash', 'mvf-videos' ),
            'featured_image'        => __( 'Featured Image', 'mvf-videos' ),
            'set_featured_image'    => __( 'Set featured image', 'mvf-videos' ),
            'remove_featured_image' => __( 'Remove featured image', 'mvf-videos' ),
            'use_featured_image'    => __( 'Use as featured image', 'mvf-videos' ),
            'insert_into_item'      => __( 'Insert into Video', 'mvf-videos' ),
            'uploaded_to_this_item' => __( 'Uploaded to this Video', 'mvf-videos' ),
            'items_list'            => __( 'Videos list', 'mvf-videos' ),
            'items_list_navigation' => __( 'Videos list navigation', 'mvf-videos' ),
            'filter_items_list'     => __( 'Filter Videos list', 'mvf-videos' ),
        );
        $args = array(
            'label'                 => __( 'Video', 'mvf-videos' ),
            'description'           => __( 'Add videos to your posts', 'mvf-videos' ),
            'labels'                => $labels,
            'supports'              => false,
            'hierarchical'          => false,
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => false,
            'can_export'            => false,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => false,
            'capability_type'       => 'page',
        );
        register_post_type( 'videos', $args );
    }
    public function add_shortcode( $atts ) {
        global $wp_embed;
        extract( shortcode_atts( array(
            'id' => '',
            'border_color' => '',
            'border_width' => '',
        ), $atts ) );
        $border_width = (($border_width !== "")? $border_width : '8px');
        $border_color = (($border_color !== "")? $border_color : '#A3A3A3');
        $options = array(
            'mvf_video_field_title',
            'mvf_video_field_subtitle',
            'mvf_video_field_description',
            'mvf_video_field_video_id',
            'mvf_video_field_type'
        );

        $video_codes = array(
            'youtube' => '[embed height=200]https://www.youtube.com/watch?v=%s[/embed]', 
            'dailymotion' => '[embed height=200]https://www.dailymotion.com/video/%s[/embed]',
            'vimeo' => '[embed height=200]https://vimeo.com/%s[/embed]'
        );
        $description = get_post_meta($id, 'mvf_video_field_description', true);
        $title = get_post_meta($id, 'mvf_video_field_title', true);
        $subtitle = get_post_meta($id, 'mvf_video_field_subtitle', true);
        $video_type = get_post_meta($id, 'mvf_video_field_type', true);
        $video_id = get_post_meta($id, 'mvf_video_field_video_id', true);

        $video_code = str_replace('%s', $video_id, $video_codes[$video_type]);

        $html = '<div class="mvf_video_wrapper" style="border: '.$border_width.'px solid '.$border_color.'">';
        $html .= '<div class="mvf_video_thumb">';
        $html .= $wp_embed->run_shortcode( $video_code );
        
        $html .= '</div>';
        $html .= '<div class="mvf_video_content">';
        $html .= '<h3 class="mvf_video_title">'.$title.'</h3>';
        $html .= '<h4 class="mvf_video_subtitle">'.$subtitle.'</h4>';
        $html .= '<p class="mvf_video_description">'.$description.'</p>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }
    public function add_button( $plugin_array ) {
      $plugin_array['mvfVideosBtn'] = plugins_url( '/assets/js/tinymce-button.js',__FILE__ );
      return $plugin_array;
    }
    public function register_button( $buttons ) {
      array_push( $buttons, 'videos' );
      return $buttons;
    }
    /* Meta box setup function. */
    public function add_meta_boxes_setup() {
        /* Add meta boxes on the 'add_meta_boxes' hook. */
        add_action( 'add_meta_boxes', array($this, 'add_meta_boxes') );
    }
    /* Create one or more meta boxes to be displayed on the post editor screen. */
    public function add_meta_boxes() {
        add_meta_box(
        'mvf-videos', // Unique ID
        __( 'Add new video', 'mvf_video_title' ),    // Title
        array($this, 'metabox_settings_cb'),   // Callback function
        'videos', // Admin page (or post type)
        'normal', // Context
        'high' // Priority
        );
    }
    public function get_fields(){
        $prefix = 'mvf_video_field_';
        $fields = array(
            array(
            'label'=>  __('Title', 'mvf-videos'),
            'desc'  => __('Video title', 'mvf-videos'),
            'id'    => $prefix.'title',
            'type'  => 'text'
            ),
            array(
                'label'=>  __('Subtitle', 'mvf-videos'),
                'desc'  => __('Video subtitle', 'mvf-videos'),
                'id'    => $prefix.'subtitle',
                'type'  => 'text'
            ),
            array(
                'label'=>  __('Description', 'mvf-videos'),
                'desc'  => __('Video description', 'mvf-videos'),
                'id'    => $prefix.'description',
                'type'  => 'textarea'
            ),
            array(
                'label'=> __('Video ID', 'mvf-videos'),
                'desc'  => __('Paste your video ID', 'mvf-videos'),
                'id'    => $prefix.'video_id',
                'type'  => 'text'
            ),
            array(
                'label'=> __('Type', 'mvf-videos'),
                'desc'  => __('Type of video', 'mvf-videos'),
                'id'    => $prefix.'type',
                'type'  => 'radio',
                'options' => array(
                        'youtube', 
                        'dailymotion',
                        'vimeo'
                        )
            )
        );
        return $fields;
    }
    public function set_videos_title( $data , $postarr ) {
        if($data['post_type'] == 'videos') {
          $title = get_post_meta($post->ID, 'mvf_video_field_title', true);
          $data['post_title'] = $post_title;
        }
        return $data;
    }
      
    public function mvf_tinymce_plugin_add_locale($locales) {
        $locales ['mvfVideosBtn'] = plugin_dir_path ( __FILE__ ) . 'includes/tinymce-btn-langs.php';
        return $locales;
    }
    public function metabox_settings_cb(){
        global $post;
        $fields = $this->get_fields();

        // Use nonce for verification
        echo '<input type="hidden" name="mvf_post_class_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';

            echo '<table class="form-table">';
            foreach ($fields as $field) {

                $meta = get_post_meta($post->ID, $field['id'], true);
                
                echo '<tr>
                        <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                        <td>';
                        switch($field['type']) {
                            case 'textarea':
                                echo '<textarea id="'.$field['id'].'" name="'.$field['id'].'">'.$meta.'</textarea><br />
                                      <span class="description">'.$field['desc'].'</span><br/>';
                            break;
                            case 'text':
                                echo '<input type="text" id="'.$field['id'].'" name="'.$field["id"].'" value="'.$meta.'" /><br />
                                <span class="description">'.$field['desc'].'</span><br/>';
                            break;
                            case 'radio':
                                echo '<p>';
                                foreach($field['options'] as $video_type){
                                    echo '<label><input name="'.$field["id"].'" type="radio" value="'.$video_type.'" '.(($meta == $video_type) ? 'checked="checked"' : '').'>'.$video_type.'</label><br />';
                                }
                                echo '</p>';
                            break;
                        } //end switch
                echo '</td></tr>';
            } // end foreach
            echo '</table>'; // end table
        }
        
        /* Save the meta box's post metadata. */
        public function mvf_save_video( $post_id ) {
            global $post;
            $fields = $this->get_fields();
          /* Verify the nonce before proceeding. */
          if ( !isset( $_POST['mvf_post_class_nonce'] ) || !wp_verify_nonce( $_POST['mvf_post_class_nonce'], basename( __FILE__ ) ) )
            return $post_id;
        
          /* Get the post type object. */
          $post_type = get_post_type_object( $post->post_type );
        
          /* Check if the current user has permission to edit the post. */
          if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;
            $options = array(
                'mvf_video_field_title',
                'mvf_video_field_subtitle',
                'mvf_video_field_description',
                'mvf_video_field_video_id',
                'mvf_video_field_type'
            );
          /* Get the posted data and sanitize it for use as an HTML class. */
          foreach ($options as $key) {
            $new_meta_value = ( isset( $_POST[$key] ) ? sanitize_html_class( $_POST[$key] ) : '' );  
            /* Get the meta key. */
          $meta_key = $key;
        
          /* Get the meta value of the custom field key. */
          $meta_value = get_post_meta( $post_id, $meta_key, true );
        
          /* If a new meta value was added and there was no previous value, add it. */
          if ( $new_meta_value && '' == $meta_value )
            add_post_meta( $post_id, $meta_key, $new_meta_value, true );
        
          /* If the new meta value does not match the old value, update it. */
          elseif ( $new_meta_value && $new_meta_value != $meta_value )
            update_post_meta( $post_id, $meta_key, $new_meta_value );
        
          /* If there is no new meta value but an old value exists, delete it. */
          elseif ( '' == $new_meta_value && $meta_value )
            delete_post_meta( $post_id, $meta_key, $meta_value );
          }
          /* Start adding Styling Meta Box To DB*/
          // verify nonce
          if (!wp_verify_nonce($_POST['mvf_post_class_nonce'], basename(__FILE__))) 
              return $post_id;
          // check autosave
          if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
              return $post_id;
          // check permissions
          if ('page' == $_POST['post_type']) {
              if (!current_user_can('edit_page', $post_id))
                  return $post_id;
              } elseif (!current_user_can('edit_post', $post_id)) {
                  return $post_id;
          }
          
          // loop through fields and save the data
          foreach ($fields as $post_data) {
              $old = get_post_meta($post_id, $post_data['id'], true);
              $new = (isset($_POST[$post_data['id']]) ? $_POST[$post_data['id']] : '');
              if ($new && $new != $old) {
                  update_post_meta($post_id, $post_data['id'], $new);
              } elseif ('' == $new && $old) {
                  delete_post_meta($post_id, $post_data['id'], $old);
              }
          }
            
          
        }
        public function modify_post_content( $post_id ) {
            $post_type = get_post_type($post_id);
            if($post_type == 'videos'){
                $video_title = get_post_meta($post_id, 'mvf_video_field_title', true);
                $post = array(
                    'ID'       => $post_id,
                    'post_title'   => $video_title,
                );
                
                remove_action( 'save_post', array( $this, 'modify_post_content' ) );
                wp_update_post( $post );
                add_action( 'save_post', array( $this, 'modify_post_content' ) );
            }
        }

}

endif;

$MVFVIDEOS = new MVF_VIDEOS;
$MVFVIDEOS->init();