(function() {
  tinymce.create('tinymce.plugins.mvfVideosBtn', {
    init: function(editor, url){
      plugins: "colorpicker",
      editor.addButton('videos', {
          title: 'Insert Shortcode',
          cmd: 'mvfVideosBtn',
          language :"en_EN",
          image: url + '/play_button_icon.png',
      }),
      editor.addCommand('mvfVideosBtn', function(){
          var selectedText = editor.selection.getContent({format: 'html'});
          var win = editor.windowManager.open({
            title: editor.getLang('mvfVideosBtn.video settings'),
            body: [
              {
                type: 'textbox',
                name: 'post_id',
                label: editor.getLang('mvfVideosBtn.post id'),
                minWidth: 200,
                value: ''
              },
              {
                type: 'textbox',
                name: 'border_width',
                label: editor.getLang('mvfVideosBtn.border width'),
                minWidth: 200,
                value: '8'
              },
              {
                type: 'colorbox',
                name: 'border_color',
                label: editor.getLang('mvfVideosBtn.border color'),
                minWidth: 200,
                value: '#000000',
                onaction: createColorPickAction(),
              }
            ],
          buttons: [
          {
            text: editor.getLang('mvfVideosBtn.insert shortcode'),
            subtype: "primary",
            onclick: function() {
              win.submit();
            }
          },
          {
              text: editor.getLang('mvfVideosBtn.cancel'),
              onclick: function() {
                win.close();
              }
          }],
          onsubmit: function(e){
              var params = [];
              if( e.data.post_id.length > 0 ) {
                params.push('id="' + e.data.post_id + '"');
              }
              if( e.data.border_color.length > 0 ) {
                params.push('border_color="' + e.data.border_color + '"');
              }
              if( e.data.border_width.length > 0 ) {
                params.push('border_width="' + e.data.border_width + '"');
              } else {
                  params.push('border_width="' + 8 + '"');  
              }
              if( params.length > 0 ) {
                paramsString = ' ' + params.join(' ');
              }
              var returnText = '[prefix_video '+paramsString+']';
              editor.execCommand('mceInsertContent', 0, returnText);
          }
        }); //endwin
      }); //editor add command
      
    },//init
    getInfo: function() {
      return {
        longname: 'MVF Video Btn',
        author: 'Hamza Abd El-Wahab',
        authorurl: 'http://www.hubahamza.me',
        version: "1.0"
      };
    }
  }); //tinymce create
  tinymce.PluginManager.add( 'mvfVideosBtn', tinymce.plugins.mvfVideosBtn);

  
  var editor = tinymce.activeEditor;
  function createColorPickAction() {
      var colorPickerCallback = editor.settings.color_picker_callback;

      if (colorPickerCallback) {
          return function() {
              var self = this;

              colorPickerCallback.call(
                  editor,
                  function(value) {
                      self.value(value).fire('change');
                  },
                  self.value()
              );
          };
      }
  }
})();