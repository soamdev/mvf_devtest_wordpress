<?php
// This file is based on wp-includes/js/tinymce/langs/wp-langs.php

if ( ! defined( 'ABSPATH' ) )
    exit;

if ( ! class_exists( '_WP_Editors' ) )
    require( ABSPATH . WPINC . '/class-wp-editor.php' );

function add_tinymce_plugin_translation() {
    $strings = array(
        'video settings' => __('Video Settings', 'mvf-videos'),
        'post id' => __('Post ID', 'mvf-videos'),
        'border width' => __('Border Width', 'mvf-videos'),
        'border color' => __('Border Color', 'mvf-videos'),
        'insert shortcode' => __('Insert', 'mvf-videos'),
        'cancel' => __('Cancel', 'mvf-videos'),
    );
    $locale = _WP_Editors::$mce_locale;
    $translated = 'tinyMCE.addI18n("' . $locale . '.mvfVideosBtn", ' . json_encode( $strings ) . ");\n";

     return $translated;
}

$strings = add_tinymce_plugin_translation();